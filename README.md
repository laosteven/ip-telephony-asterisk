# IP Telephony with Asterisk #
-------

Using the **[Asterisk](https://en.wikipedia.org/wiki/Asterisk_(PBX))** software under *Ubuntu 12.04*, customize various configuration files in order to understand:

* **SIP** protocol;
* **IAX2** protocol;
* Asterisk servers connection;
* **IVR** interactive menu.

Different case scenarios have to be reproduced. Meaning that the phone has to use a **custom ringtone**, **play** a specific message once dialing a phone extension or even **record** a complete phone conversation.

# Objectives #
-------
* Explore the endless possibilities of Asterisk's commands.
* Understand IP telephony **server architecture**.
* Configure an IP telephone.

# How do I get set up? #
-------
* Make a backup of current configuration folder.
* Download all the files in the project's root folder.
* Replace all the `.conf` files with downloaded files.  

# For more information #
-------
Visit the following website: [**Telecommunication networks** (GTI610)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=gti610) [*fr*]